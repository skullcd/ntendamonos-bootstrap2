<?php
  require("conex.php");
  $con = conexion();
  $palabra_id = $_GET["palabra_id"];
  $datos = [];
  // $palabra_id = 13;
  $filename = "../img/imgPalabras/{$palabra_id}/";
  if (file_exists($filename)) {
    $ficheros1  = scandir($filename);
    $datos [0] = $ficheros1;
    $qry = "SELECT i.autor, i.descripcion, u.name, i.idImagen
    FROM imagen i
    INNER JOIN users u ON i.users_id = u.id
    WHERE palabras_id = {$palabra_id} AND reporte < 3";
    $res = $con->query($qry);
    unset($ficheros1[0]);
    unset($ficheros1[1]);
    $ficheros1 = array_values($ficheros1);
    $datos_temp = [];
    while($datosP = $res->fetch_row()){
        $datos[0]= $ficheros1;
        $datos_temp[] = $datosP;
    }
    $datos[] = $datos_temp;
    echo json_encode($datos, JSON_UNESCAPED_UNICODE);
  } else {
    echo "subir_imagen";
  }
?>
