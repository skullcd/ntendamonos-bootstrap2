<?php
    require("conex.php");
    $con = conexion();
    $array = [];
    $qry = "SELECT palabra, id, id_pais FROM palabras WHERE estatus = 'correcta' ORDER BY palabra";
    $res = $con->query($qry);
    if($res->num_rows > 0){
      while($datos = $res->fetch_row()){
        $qryP = "SELECT iconoPais, id FROM pais WHERE id = {$datos[2]}";
        $resP = $con->query($qryP);
        while($datosP = $resP->fetch_row()){
            $datos[2] = $datosP[0];
            $datos[3] = $datosP[1];
        }
        $array[] = $datos;
      }
      echo json_encode($array, JSON_UNESCAPED_UNICODE);
    }else{
      echo 'ninguna';
    }
?>
