<?php
require("conex.php");
$con = conexion();
$relacion_id = $_GET["relacion_id"];
// $relacion_id = 4;
$datos = [];
$query_relacion = "SELECT descripcion, users_id FROM reporte WHERE palabras_id = {$relacion_id} AND tipo ='relacion' AND estatus = 1";
$data_relacion = $con->query($query_relacion);
while ($reporte = $data_relacion->fetch_row()) {
  $query_user = "SELECT name FROM users WHERE id = {$reporte[1]}";
  $data_user = $con->query($query_user);
  while ($user_reporte = $data_user->fetch_row()) {
    $datos[] = [$reporte[0], $user_reporte[0]];
  }
}
echo json_encode($datos, JSON_UNESCAPED_UNICODE);

?>
