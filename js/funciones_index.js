function calcularAltura(){
  altura = Math.max(document.documentElement.clientHeight, window.innerHeight);
  document.querySelector('.contenedor-principal').style.minHeight = altura+'px';
  document.querySelector('.contMenu').style.minHeight = altura+'px';
  document.querySelector('.menuDesplegable').style.minHeight = altura+'px';
}
window.addEventListener('orientationchange', calcularAltura, true);
window.addEventListener('resize', calcularAltura, true);
window.addEventListener('scroll', calcularAltura, true);
window.addEventListener('load', paginaActiva);

function paginaActiva(){
  setTimeout(function(){
    var url = window.location;
    var url = url.toString();
    var urls = url.split('/');
    var perfil = document.getElementById("perfilPag");
    var sugerencia = document.getElementById("sugerenciaPag");
    var revaluacion = document.getElementById("revaluacionPag");
    var relacion = document.getElementById("relacionPag");
    var unaPalabra = document.getElementById("unaPalabraPag");
    var perfil2 = document.getElementById("perfilPag2");
    var sugerencia2 = document.getElementById("sugerenciaPag2");
    var revaluacion2 = document.getElementById("revaluacionPag2");
    var relacion2 = document.getElementById("relacionPag2");
    var unaPalabra2 = document.getElementById("unaPalabraPag2");

    if (urls[4] == "perfil.html") {
        perfil.classList.add('_activo');
        perfil2.classList.add('paginaActiva');
    } else if (urls[4] == "aceptarSugerencia.html") {
        sugerencia.classList.add('_activo');
        sugerencia2.classList.add('paginaActiva');
    } else if (urls[4] == "aceptarReevaluacion.html") {
        revaluacion.classList.add('_activo');
        revaluacion2.classList.add('paginaActiva');
    } else if (urls[4] == "sugerirPalabra.html") {
        relacion.classList.add('_activo');
        relacion2.classList.add('paginaActiva');
    } else if (urls[4] == "agregarUnapalabra.html") {
        unaPalabra.classList.add('_activo');
        unaPalabra2.classList.add('paginaActiva');
    }

  }, 50);
}
/**
*   Function Menu
*   Muestra las opciones adecuadas dependiendo del tipo
*   de usuario
*/
$(function(){
    var cont = "";
    if(localStorage.getItem('id_usuario')){
        user = localStorage.getItem('id_usuario');
        usuario = localStorage.getItem('usuario');
        fotoPerfil = localStorage.getItem('fotoPerfil');
        tipoUsuario = localStorage.getItem('tipoUsuario');
        $("#textoBienvenida").html("¿Qué puedes hacer "+usuario+" ?");
        $("#tituloMenu").html("<h1>"+usuario+"</h1>");
        $("#imgPerM").attr("src", "img/imgPerfil/"+fotoPerfil);
        cont = "<a href='perfil.html'><div id='perfilPag2' class='opciones'><div id='perfilPag'></div><i class='fa fa-user' aria-hidden='true'></i><h2>Perfil</h2></div></a>"+
            "<a href='sugerirPalabra.html'><div id='relacionPag2' class=' opciones'><div id='relacionPag'></div><i class='fa fa-pencil' aria-hidden='true'></i><h2>Sugerir Relación</h2></div></a>";
        if(tipoUsuario == "moderador"){
           cont += "<a href='agregarUnapalabra.html'><div  id='unaPalabraPag2' class='opciones'><div id='unaPalabraPag'></div><i class='fa fa-plus' aria-hidden='true'></i><h2>Agregar Palabra</h2></div></a>"+
           "<a href='aceptarSugerencia.html'><div id='sugerenciaPag2' class='opciones'><div id='sugerenciaPag'></div><i class='fa fa-pencil-square-o' aria-hidden='true'></i><h2>Sugerencias</h2></div></a>"+
        "<a href='aceptarReevaluacion.html'><div id='revaluacionPag2' class='opciones'><div id='revaluacionPag'></div><i class='fa fa-check-circle-o' aria-hidden='true'></i><h2>Reevaluación</h2></div></a>";
           }
    }else{
        $("#textoBienvenida").html("¿Qué puedes hacer?");
        $("#tituloMenu").html("");
        $("#imgPerM").attr("src", "img/imgPerfil/default.jpg");
        cont = "";
    }
    $("#cont_menu").html(cont);
});


/**
* Function cerrarSesion
* Accese al metodo cerrarSesion para eliminar variables de sesion en php
* Si manda un correcto elimina el localStorage
*/
$(document).ready(function(){
  $("#salirSesion").click(function(event) {
      event.preventDefault();
      localStorage.removeItem('id_usuario');
        localStorage.removeItem('usuario');
        localStorage.removeItem('fotoPerfil');
        localStorage.removeItem('tipoUsuario');
        window.location = 'index.html';
  });
});

/**
* function desplegarMenu
* Muestra el menu al momento de dar click
*/
function desplegarMenu(){
  var menu2 = document.getElementById("menu2");
  var menu1 = document.getElementById("menu1");
  menu2.classList.add('desplegarMenuM2');
  menu1.classList.add('desplegarMenuM1');
}

/**
* function ocultarMenu
* Oculta el menu al momento de dar click
*/
function ocultarMenu(){
  var menu2 = document.getElementById("menu2");
  var menu1 = document.getElementById("menu1");
  menu2.classList.remove('desplegarMenuM2');
  menu1.classList.remove('desplegarMenuM1');
}
//________________________Sección de sugerencias
/**
* Function onchange Pais
*/
$(document).ready(function(){
  if($("#paisRegistro").length || $("#paisRegistro3").length){
      var con = "";
      var todos = "<option value='0'>Todos</option>";
    $.ajax({
      url: 'Backend/obtenerPaises.php',
      type:'GET',
      success: function(data){
          data = JSON.parse(data);
          for(pais in data){
              con = con + "<option value='"+data[pais][0]+"'>"+data[pais][1]+"</option>";
          }
          if ($("#paisRegistro").length) {
            $("#paisRegistro").html(con);
            $("#paisRegistro2").html(con);
            self.cargarPalabras("todos");
          } else if ($("#paisRegistro3").length) {
              con = todos + con;
              $("#paisRegistro3").html(con);
          }
      }
    });
  }
});

function buscarTodasPalabras(){
  con = "";
  $.ajax({
    url: 'Backend/buscarTodasPalabras.php',
    type:'GET',
    success: function(data){
        if(data.trim() != "ninguna"){
            data = JSON.parse(data);
            for(palabras in data){
                con += "<div onclick='resultados("+data[palabras][1]+")' class=' col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
              "<div class='banderaP'><img src='img/banderas/unesco.jpg' alt='bandera' class='banderaEspe'/></div>"+
              "<div class='text-left pais2'><h1 onclick='resultados("+data[palabras][1]+")'>"+data[palabras][0]+"</h1></div>"+
                "</div>";
            }
        }else{
            con = "<div class='text-center emptyP empty'>"+
          "<i class='fa fa-folder-open'  style='font-size: 35px; color:#528AA8'  aria-hidden='true'></i>"+
          "<h3>Sin Resultados</h3></div>";
        }
        $("#respuestas2").html(con);
    }
    });
}

$(function(){
  $("#paisRegistro").change(function(){
    cargarPalabras("uno");
  });
  $("#paisRegistro2").change(function(){
    cargarPalabras("dos");
  });
  $("#paisRegistro3").change(function(){
    id_pais = $('#paisRegistro3').val();
    if (id_pais == 0) {
      buscarTodasPalabras();
    } else {
      con = "";
      $.ajax({
        url: 'Backend/buscarPalabras.php',
        type:'GET',
        data:{id_pais:id_pais},
        success: function(data){
            if(data.trim() != "ninguna"){
                data = JSON.parse(data);
                for(palabras in data){
                    con += "<div onclick='resultados("+data[palabras][3]+")' class=' col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
                  "<div class='banderaP'><img src='img/banderas/unesco.jpg' alt='bandera' class='banderaEspe'/></div>"+
                    "<div class='text-left pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                    "<div class='text-center aceptar'></div></div>";
                }
            }else{
                con = "<div class='text-center emptyP empty'>"+
              "<i class='fa fa-folder-open'  style='font-size: 35px; color:#528AA8' aria-hidden='true'></i>"+
              "<h3>Sin Resultados</h3></div>";
            }
            $("#respuestas2").html(con);
        }
        });
    }
  });
});
/**
* Function cargarPalabras
* Obtiene las palabras disponibles
*/
function cargarPalabras(tipo){
    aux = "";
    if(tipo == "todos" || tipo == "dos"){
        aux = $('#paisRegistro').val();
       }
    if(tipo == "uno"){
        aux = $("#paisRegistro").val();
       }
  if(aux !=  ""){
    var idPais = aux;
    $.ajax({
      url: 'Backend/obtenerPalabrasPais.php',
      type:'GET',
      data: {pais:idPais},
      success: function(data){
        if(data != ""){
            data = JSON.parse(data);
            var con = "";
            for(palabras in data){
              con = con + "<option data-value='"+data[palabras][0]+"'>"+data[palabras][1]+"</option>"
            }
            if(tipo == "todos"){
                $("#relacionadaPalabra").html(con);
                $("#relacionadaPalabra2").html(con);
            }
            if(tipo == "dos"){
               $("#relacionadaPalabra2").html(con);
            }
            if(tipo == "uno"){
               $("#relacionadaPalabra").html(con);
            }

         }
      }
    });
  }
}

/**
* Function verificarDescripcion
* Muestra o no el input para ingresar la definicion de la palabra
* dependiendo de la respuesta del ajax
* @param palabra
* @param pais
* @param tipo
*/
function verificarDescripcion(palabra, pais, tipo){
  contenido_sin_relacion= "";
  contenido_con_relacion= "";
  contenido="";
  sin_relacion = [];
  relacion = [];
  var tipoDef = "";
  if(tipo=="palabra2"){
    tipoDef = "#definicion2"
  }else{
    tipoDef = "#definicion1"
  }
  pais_texto = $("#paisRegistro option[value='"+pais+"']").text();
  $.ajax({
    url: 'Backend/obtenerDescripcionPalabra.php',
    type:'GET',
    data: {palabra:palabra, pais:pais, pais_texto:pais_texto},
    success: function(data){
      if (data != "") {
          data = JSON.parse(data);
      }
          pais2 ="";
          $("#definicion_unesco").css("display","none");
          $("#definicion_ifilac").css("display","none");
          $(tipoDef).css("display","none");
          for (var i = 0; i < data.length; i++) {
            if (data[i].split(':')[1]== "no") {
              sin_relacion.push(data[i].split(':')[0]);
              pais2 = data[i].split(":")[0];
              pais2 = pais2.split('.')[0];
              contenido_sin_relacion = "<div class='pais_relacionado'><img onclick=\"relacionar_palabra(\'"+pais2+"\', \'"+palabra+"\')\"' src='img/banderas/"+data[i].split(':')[0]+"' id='id_"+pais2+"' alt=''></div>"+contenido_sin_relacion;
            } else {
              contenido_con_relacion = "<div class='pais_relacionado background_white'><img  src='img/banderas/"+data[i].split(':')[0]+"' alt=''></div>"+contenido_con_relacion;
              relacion.push(data[i].split(':')[0]);

            }
          }
          if (relacion.length == 4) {
              $(".paises_con_relacion").css("height","150px");
          } else if (relacion.length == 5 || relacion.length == 6 ) {
            $(".paises_con_relacion").css("height","170px");
          }else if (relacion.length == 7  || relacion.length == 8 || relacion.length == 9) {
            $(".paises_con_relacion").css("height","210px");
          } else if (relacion.length == 10  || relacion.length == 11 || relacion.length == 12) {
            $(".paises_con_relacion").css("height","270px");
          } else if (relacion.length == 13  || relacion.length == 14 || relacion.length == 15) {
            $(".paises_con_relacion").css("height","340px");
          }

          if (relacion.length == data.length) {
              $(".paises_sin_relacion").css("display","none");
              $(".con_relacion").html(contenido_con_relacion);
              $(".paises_con_relacion").css("display","block");
          } else if (sin_relacion.length == data.length) {
            $(".paises_con_relacion").css("display","none");
            $(".sin_relacion").html(contenido_sin_relacion);
            $(".paises_sin_relacion").css("display","block")
            $(tipoDef).css("display","block");
            $("#definicion_unesco").css("display","block");
            $("#definicion_ifilac").css("display","block");
          } else {
            $(".con_relacion").html(contenido_con_relacion);
            $(".paises_con_relacion").css("display","block");
            $(".sin_relacion").html(contenido_sin_relacion);
            $(".paises_sin_relacion").css("display","block")
          }
    }
  });
}


function relacionar_palabra(pais, palabra){
  $(".pais_relacionado img").css("height","100%");
  $(".pais_relacionado img").css("width","100%");
  $("#palabra2").css("display","block");
  $("#paisRegistro2").attr("value",pais);
  $(".pais_relacionado img").css("opacity",".5");
  $("#id_"+pais).css("opacity","1");
  $("#id_"+pais).css("height","110%");
  $("#id_"+pais).css("width","110%");
}

$(function(){
    $("#palabra").blur(function(){
       var palabra = $("#palabra").val();
        var pais = $("#paisRegistro").val();
        if(palabra != ""){
          self.verificarDescripcion(palabra, pais, "palabra1");
        } else {
          $("#definicion1").css("display","none");
          $("#definicion_ifilac").css("display","none");
          $("#definicion_unesco").css("display","none");
        }
    });
    $("#paisRegistro").blur(function(){
       var palabra = $("#palabra").val();
        var pais = $("#paisRegistro").val();
        if(palabra != ""){
          self.verificarDescripcion(palabra, pais, "palabra1");
        } else {
          $("#definicion1").css("display","none");
          $("#definicion_ifilac").css("display","none");
          $("#definicion_unesco").css("display","none");
        }
    });
});
/**
* Function focus Palabra2
* Sube el contenedor para apreciarlo mejor
*/
$(document).ready(function(){
  $("#palabra2").focus(function(){
    $("#mover").css('top','-150px');
  });
  $("#definicion2").focus(function(){
    $("#mover").css('top','-150px');
  });
});

/**
* Function blur Palabra2
* Sube el contenedor para apreciarlo mejor
*/
$(document).ready(function(){
  $("#palabra2").blur(function(){
    $("#mover").css('top','80px');
  });
  $("#definicion2").blur(function(){
    $("#mover").css('top','80px');
  });
});

/**
* Function enviarPalabras
* Recupera los datos y los inserta en la BD
*/
$(document).ready(function(){
  $("#mandarSugerencia").click(function(){
    var palabraSugerida = $("#palabra").val().toUpperCase();
    if(palabraSugerida != ""){
      var user = localStorage.getItem('id_usuario');
      var paisSugerida = $("#paisRegistro").val();
      var paisRelacion = $("#paisRegistro2").val();
      var palabraRelacion = $("#palabra2").val().toUpperCase();
      var definicion1 = "";
      if($("#definicion1").css("display")=="block"){
        if($("#definicion1").val() != "" || $("#definicion_ifilac").val() != "" || $("#definicion_unesco").val() != ""){
          definicion1 = $("#definicion1").val();
          definicion_ifilac=$("#definicion_ifilac").val();
          definicion_unesco= $("#definicion_unesco").val();
        }else{
          self.palabra("Complete todos los campos");
          return;
        }
      }else{
        definicion1 = "ninguna def";
        definicion_ifilac= "ninguna def";
        definicion_unesco=  "ninguna def";
      }
      $.ajax({
        url: 'Backend/guardarPalabra.php',
        type:'GET',
        data: {palabraSugerida:palabraSugerida, paisSugerida:paisSugerida, definicion_ifilac:definicion_ifilac, definicion_unesco:definicion_unesco, definicion1:definicion1, palabraRelacion:palabraRelacion, paisRelacion: paisRelacion, usuario:user},
        success: function(data){
          console.log(data);
          if(data.trim() == "Exitoso"){
            $("#palabra").val('');
            $("#palabra2").val('');
            $("#definicion1").val('');
            $("#definicion_ifilac").val('');
            $("#definicion_unesco").val('');
            $("#definicion1").css("display","none");
            $("#definicion_ifilac").css("display","none");
            $("#definicion_unesco").css("display","none");
            $(".paises_con_relacion").css("display","none");
            $(".paises_sin_relacion").css("display","none");
            $(".espacio-espera").css("display","block");

            self.palabra("Gracias");
          }
          if(data.trim() == "Duplicado"){
            self.palabra("Relación existente");
          }
          if(data.trim() == "Error"){
            self.palabra("No se puede relacionar asi misma");
          }
        }
      });
    }else{
      self.palabra("Complete todos los campos");
    }
  });
});




/**
* Function enviarPalabras
* Recupera los datos y los inserta en la BD
*/
$(document).ready(function(){
  $("#mandarPalabra").click(function(){
    var palabraSugerida = $("#palabra").val().toUpperCase();
    if(palabraSugerida != ""){
      var user = localStorage.getItem('id_usuario');
      var paisSugerida = $("#paisRegistro").val();
      var definicion1 = "";
      if($("#definicion1").css("display")=="block"){
        if($("#definicion1").val() != ""){
          definicion1 = $("#definicion1").val();
          definicion_ifilac=$("#definicion_ifilac").val();
          definicion_unesco= $("#definicion_unesco").val();
          $.ajax({
            url: 'Backend/guardarUnaPalabra.php',
            type:'GET',
            data: {palabraSugerida:palabraSugerida, paisSugerida:paisSugerida, definicion_ifilac:definicion_ifilac, definicion_unesco:definicion_unesco, definicion1:definicion1, usuario:user},
            success: function(data){
              if(data.trim() == "Exitoso"){
                $("#palabra").val('');
                $("#definicion1").val('');
                $("#definicion_ifilac").val('');
                $("#definicion_unesco").val('');
                $("#definicion1").css("display","none");
                $("#definicion_ifilac").css("display","none");
                $("#definicion_unesco").css("display","none");
                $(".paises_con_relacion").css("display","none");
                $(".paises_sin_relacion").css("display","none")
                self.palabra("Gracias");
              }
            }
          });


        }else{
          self.palabra("Complete todos los campos");
          return;
        }
      }else{
        self.palabra("La palabra ya esta registrada");
      }
    }
  });
});





/**
* function esconderBusqueda
* Oculta el bloque de busqueda de Palabra
*/
function esconderBusqueda(){
  var busqueda = document.getElementById("busqueda");
  busqueda.style.display = "none";
    var aux = "";
    if(localStorage.getItem("usuario")){
        aux = localStorage.getItem("usuario");
       }
    con = '<div class="respuestas" id="respuestas">'+
          '<div class="imagenIndex"><img src="img/team.png" alt="team"></div>'+
          '<div class="titulos"><h1 id="textoBienvenida">¿Qué puedes hacer '+aux+' ?</h1>'+
            '<p>Mantenerte siempre comunicado</p><p>Estés donde estés</p></div></div>';
    $("#contenedorEspecial").html(con);
}

/**
* function esconderBusqueda
* Oculta el bloque de busqueda de Palabra
*/
function esconderBusquedaPalabras(){
  var busqueda = document.getElementById("busqueda");
  busqueda.style.display = "none";
    var aux = "";
    if(localStorage.getItem("usuario")){
        aux = localStorage.getItem("usuario");
       }
    // con = '<div class="respuestas" id="respuestas">'+
    //       '<div class="imagenIndex"><img src="img/team.png" alt="team"></div>'+
    //       '<div class="titulos"><h1 id="textoBienvenida">¿Qué puedes hacer '+aux+' ?</h1>'+
    //         '<p>Mantenerte siempre comunicado</p><p>Estés donde estés</p></div></div>';
    // $("#contenedorEspecial").html(con);
}
/**
* function mostrarBusqueda
* Muestra el cuadro de busqueda de palabras
*/
function mostrarBusqueda(){
  var busqueda = document.getElementById("busqueda");
  busqueda.style.display = "block";
}

/***************Busqueda Palabras **********/
/**
* Function busqueda
* Obtiene la palabra a buscar
* Valida si se escribio algo en el input si no muestra mensaje
* Valida si se recibe algo del controlador si no muestra imagen de vacio
* Si se encontraron resultados muestra la palabra con su pais
*/
$(document).ready(function(){
  $("#busqueda2").click(function(){
    var palabra = $("#palabraBuscar").val().toUpperCase();
      var con = "";
    if(palabra == ""){
      self.palabra("Ingrese una palabra");
    }else{
      $.ajax({
        url: 'Backend/buscarPalabra.php',
        type:'GET',
        data: {palabra:palabra},
        success: function(data){
            if(data.trim() != "ninguna"){

                data = JSON.parse(data);
                for(palabras in data){
                    con +="<div onclick='resultados("+data[palabras][0]+")' class='busquedaAc col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
                "<div class='banderaP'><img src='img/banderas/"+data[palabras][2]+"' alt='bandera' class='banderaEspe'/></div>"+
                  "<div class='text-left pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                  "<div class='text-center aceptar'></div></div>";
                }
            }else{
                con = "<div class='text-center emptyP empty'>"+
              "<i class='fa fa-folder-open'  style='font-size: 35px; color:#528AA8'  aria-hidden='true'></i>"+
              "<h3>Sin Resultados</h3></div>";
            }
            $(".contenedor-principal").html(con);
        }
      });
    }
  });
});


function CargarDatalist(){
  $.ajax({
    url: 'Backend/obtenerPalabrasDatalist.php',
    type:'GET',
    data: {},
    success: function(data){
      data = JSON.parse(data);
      con = "";
      contenido = "";
      conta = 0;
      for(datos in data){
      con += "<option value = '"+data[datos]+"'></option>";
      conta++;
      }
      contenido += "<datalist id='palabras'>"+con+"</datalist>";
      $("#palabraBuscar2").html(contenido);
      $("#palabraBuscar").html(contenido);
    }
  });
}

$(document).ready(function(){
  $("#busqueda3").click(function(){
    var palabra = $("#palabraBuscar2").val().toUpperCase();
      var con = "";
    if(palabra == ""){
      self.palabra("Ingrese una palabra");
    }else{
      $.ajax({
        url: 'Backend/buscarPalabra.php',
        type:'GET',
        data: {palabra:palabra},
        success: function(data){
            if(data.trim() != "ninguna"){
                data = JSON.parse(data);
                for(palabras in data){
                  con += "<div  onclick='resultados("+data[palabras][0]+")' class='busquedaAc col-sm-5 col-md-5 col-lg-5 respuesta sugeridas'>"+
                  "<div class='banderaP'><img src='img/banderas/unesco.jpg' alt='bandera' class='banderaEspe'/></div>"+
                    "<div class='text-left pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                    "<div class='text-center aceptar'></div></div>";
                }
            }else{
                con = "<div class='empty'>"+
              "<i class='fa fa-folder-open'  style='font-size: 35px; color:#528AA8'  aria-hidden='true'></i>"+
              "<h3>Sin Resultados</h3></div>";
            }
            $(".contenedor-principal").html(con);
        }
      });
    }
  });
});

// /**
// * Function busqueda
// * Obtiene la palabra a buscar
// * Valida si se escribio algo en el input si no muestra mensaje
// * Valida si se recibe algo del controlador si no muestra imagen de vacio
// * Si se encontraron resultados muestra la palabra con su pais
// */
// $(document).ready(function(){
//   $("#paisRegistro").change(function(){
//     var pais = document.getElementById("paisRegistro");
//       $.ajax({
//         url: 'Backend/buscarPalabras.php',
//         type:'GET',
//         data: {pais:pais},
//         success: function(data){
//             if(data != "ninguna"){
//                 data = JSON.parse(data);
//                 for(palabras in data){
//                     con += "<div class='respuesta sugeridas'>"+
//                     "<img src='img/banderas/"+data[palabras][2]+"' alt='bandera' class='banderaEspe'/>"+
//                       "<div class='pais2'><h1>"+data[palabras][1]+"</h1></div>"+
//                       "<div class='aceptar'><img src='img/next.png' alt='aceptar' onclick='resultados("+data[palabras][0]+")'></div></div>";
//                 }
//             }else{
//                 con = "<div class='empty'>"+
//               "<img src='img/clear.png' alt='empty' />"+
//               "<h3>Sin Resultados</h3></div>";
//             }
//             $("#respuestas").html(con);
//         }
//       });
//   });
// });

/**************** Resultados ***********/
/**
* Function ocultarDefinicion
* Oculta la definicion de la palabra
*/
function ocultarDefinicion(){
  $("#reporte").css('visibility','hidden');
  $("#reporte").css('opacity','0');
  $("#definicion").css('visibility','hidden');
}

/**
* Function mostrarDefinicion
* Muestra la definicion de la palabra
*/
function mostrarDefinicion(){
  $("#reporte").css('visibility','visible');
  $("#reporte").css('opacity','1');
  $("#definicion").css('visibility','visible');
}

/**
* Function ocultarReporte
* Oculta el div del reporte
*/
function ocultarReporte(){
  $("#reporte").css('visibility','hidden');
  $("#reporte").css('opacity','0');
  if($("#definicion").css('visibility')=='visible'){
    $("#definicion").css('visibility','hidden');
  }
  $("#reportein").css('visibility','hidden');
}
/**
* Function resultados
* Redirecciona a la pagina en especifico con su id
* @param id
*/
function resultados(id){
    sessionStorage.setItem("idPalabra",id);
    window.location = "resultados.html";
}

/**
* Function retrocederEspB
* De la vista de detalles de la palabra
* redirecciona al index
*/
function retrocederEspB(){
  window.location = "palabras.html";
}

/**
* Function retrocederEspB
* De la vista de detalles de la palabra
* redirecciona al index
*/
function retrocederRe(){
  window.location = "aceptarReevaluacion.html";
}

/**
* Function retrocederEspB
* De la vista de detalles de la palabra
* redirecciona al index
*/
function retrocederSug(){
  window.location = "aceptarSugerencia.html";
}


/**resultados de palabras*/
function obtenerDatosPalabra(){
    // self.palabra("Cargando....");
    var con = "";
    idPalabra = sessionStorage.getItem("idPalabra");
    $.ajax({
        url: 'Backend/obtenerPalabrasBusqueda.php',
        type:'GET',
        data: {palabra:idPalabra},
        success: function(data){
            data = JSON.parse(data);
            var def1 = "";
            var def2 = "";
            var def3 = "";
            def1 += data[0][6];
            def2 += data[0][7];
            def3 += data[0][8];
            if(data.length > 1){
             for(datos in data){
                if(datos == 0){
                   if (def1 == "null") {def1 = "No hay Definición";}
                   if (def2 == "null") {def2 = "No hay Definición";}
                   if (def3 == "null") {def3 = "No hay Definición";}
                   $("#definicion_ifilac").html(def2);
                   $("#definicion_unesco").html(def3);
                   $("#definicion_general").html(def1);
                   $("#banderaPrincipal").attr("src", "img/banderas/"+data[datos][10]);
                    $("#palabraPadreB").html(data[datos][1]);
                    $("#palabraPadreId").val(data[datos][0]);
                    $("#sTitulo").attr("onclick", "hablar('"+data[datos][1]+"',this,1)");
                }else{
                    con += "<div class='col-xs-12 col-sm-8 col-md-5 col-lg-3 text-center sugerenciaTarjeta' id='"+data[datos][0]+"'>";
                    con += "<div class='banderaR'><img src='img/banderas/"+data[datos][2]+"' alt='bandera' class='banderaEspe'/></div>"+
                    "<div class='text-left pais2'><h2 class='text-left'>"+data[datos][1]+"</h2></div>"+
                    "<div class='voz'><i "+
                    'onclick="hablar(\''+data[datos][1]+'\',this,2)"'+
                     "class='fa fa-volume-up' style='font-size: 35px; color:#528AA8; margin-top:10px;' /></div></div>";
                }
            }
           }else{
             if (def1 == "null") {def1 = "No hay Definición";}
             if (def2 == "null") {def2 = "No hay Definición";}
             if (def3 == "null") {def3 = "No hay Definición";}
             $("#definicion_ifilac").html(def2);
             $("#definicion_unesco").html(def3);
             $("#definicion_general").html(def1);
             $("#banderaPrincipal").attr("src", "img/banderas/"+data[0][10]);
                $("#palabraPadreB").html(data[0][1]);
                $("#palabraPadreId").val(data[0][0]);
                $("#sTitulo").attr("onclick", "hablar('"+data[0][1]+"',this,1)");
                    con = "<div class='text-center empty'>"+
              "<i class='fa fa-folder-open' style='font-size: 35px; color:#528AA8' aria-hidden='true'></i>"+
              "<h3>Sin Relaciones</h3></div>";
           }
            $("#resultadosBusqueda").html(con);
        }
      });
}

/**
* Function hablar
* Obtiene la palabra para poder usar la API responsiveVoice
* Se cambia el css de los iconos y reproduce la palabra
* @param  palabra
* @param  objeto
* @param tipo
*/
function hablar(palabra, objeto, tipo){
  if(tipo==1){
    objeto.style.opacity = "0.3";
  }else{
    objeto.style.background = "rgba(70, 126, 155, 0.3)";
  }
  palabra = palabra.toLowerCase();
  responsiveVoice.setDefaultVoice("Spanish Female");
  responsiveVoice.speak(palabra);
  setTimeout(function(){
    responsiveVoice.cancel();
    if(tipo==1){
      objeto.style.opacity = "1";
    }else{
        objeto.style.background = "transparent";
    }
   }, 2000);
}

/************Aprobar Sugerencias ****/
/**
* Function Sugerencias
* Manda un Ajax a sugerencuas/propuestas
* Obtiene los datos de las palabras y los inserta en un div
* el cual se agrega al contenedor donde se mostraran los resultados
* Si no retorna informacion mostrara una imagen de sin sugerencias
*/
$(document).ready(function(){
    if($("#sugerencias").length){
        usuario = localStorage.getItem('id_usuario')
      $.ajax({
        url: 'Backend/obtenerPropuestas.php',
        type:'GET',
          data: {usuario:usuario},
        success: function(data){
            data = JSON.parse(data);
          var con = "";
          if(data.length > 0){
            for(palabras in data){
                con += "<div  onclick='cambiar("+data[palabras][0]+")' class=' col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
                "<div class='banderaP'><img src='img/banderas/"+data[palabras][2]+"' alt='bandera' class='banderaEspe'/></div>"+
                  "<div class='text-left pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                  "<div class='text-center aceptar'></div></div>";
            }

          }else{
            con = "<div class='text-center empty'>"+
              "<img src='img/empty.png' alt='empty' />"+
              "<h3>Sin Sugerencias</h3></div>";
          }
          $("#respuestas").html(con);
        }
      });
    }
});
/**
* Function cambiar
* @param id
* Redirige a la ruta sugerencias/especifica
* con el id de la palabra seleccionada
*/
function cambiar(id){
    sessionStorage.setItem("idPalabra",id);
  window.location = "especificSugerencia.html";
}
/**
* Function obtenerEspecificos
* Obtiene las relaciones sugeridas con dicha palabra seleccionada
* Inserta los datos retornados en divs los cuales se agregan al contenedor
* contenedorSugerencias para mostrar los resultados
*/
$(document).ready(function(){
  if($("#palabraPadre").length){
      // self.palabra("Cargando...");
    var id = sessionStorage.getItem("idPalabra");
    if(id != ""){
        usuario = localStorage.getItem('id_usuario');
      $.ajax({
        url: 'Backend/obtenerPropuestasEspecifico.php',
        type:'GET',
        data: {id:id, usuario:usuario},
        success: function(data){
            data = JSON.parse(data);
            var def1 = "";
            var def2 = "";
            var def3 = "";
            var con = "";
            def1 += data[1][6];
            def2 += data[1][7];
            def3 += data[1][8];
          for(datos in data){
              if(datos == 0){
                   if (def1 == "null") {def1 = "No hay Definición";}
                   if (def2 == "null") {def2 = "No hay Definición";}
                   if (def3 == "null") {def3 = "No hay Definición";}

                   $("#definicion_ifilac3").html(def2);
                   $("#definicion_unesco3").html(def3);
                   $("#definicion_general3").html(def1);
                   $("#banderaPrincipal3").attr("src", "img/banderas/"+data[0][2]);
                    $("#palabraPadre").html(data[datos][1]);
                    $("#palabraPadreId").val(data[datos][0]);
                    $("#sTitulo").attr("onclick", "hablar('"+data[datos][1]+"',this,1)");

                   // $("#palabraDefinicion").html(aux);
                   //  $("#palabraPadre").html(data[datos][1]);
                   //  $("#palabraPadreId").val(data[datos][0]);
                    $("#sTitulo").attr("onclick", "hablar('"+data[datos][1]+"',this,1)");
                }else{
                    con = con + "<div class=' col-xs-12 col-sm-6 col-md-6 col-lg-3 acomodarS sugerenciaTarjeta' id='"+data[datos][5]+"'>"+
                    "<div class=' banderaS'><img src='img/banderas/"+data[datos][2]+"' alt='bandera' class='banderaEspe'/></div>"+
                    "<div class='text-left pais2'><h2>"+data[datos][1]+"</h2></div>"+
                    "<i onclick='aceptar("+data[datos][5]+")' style='color:#56B892' class='fa fa-check-circle-o' aria-hidden='true'></i>"+
                    "<i onclick='rechazar("+data[datos][5]+")' style='color:#EC575D' class='fa fa-times-circle-o' aria-hidden='true'></i></div>";
                    // "<img src='img/aceptar.png' alt='aceptar' onclick='aceptar("+data[datos][5]+")' class='opcionesSug aceptar'/>"+
                    // "<img src='img/cancel.png' alt='rechazar' onclick='rechazar("+data[datos][5]+")' class='opcionesSug rechazar'/></div>";
                }
          }
          $("#contenedorSugerencias").html(con);
        }
      });
    }
  }
});


/**
* Function aceptar
* Si se acepta la relación se manda un ajax a la ruta /aceptarPropuestas
* Si retorna un correcto se elimina el div del contenedorSugerencias
* Si el div queda vacio se retorna a /sugerencias
* @param id
*/
function aceptar(id){
    usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/aceptarPropuesta.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data.trim() == "correcto"){
        $("#"+id).remove();
        if($("#contenedorSugerencias").is(':empty')){
          window.location = "aceptarSugerencia.html";
        }
      }
    }
  });
}
/**
* Function rechazar
* Si se rechaza la relación se manda un ajax a la ruta /rechazarPropuestas
* Si retorna un correcto se elimina el div del contenedorSugerencias
* Si el div queda vacio se retorna a /sugerencias
* @param id
*/
function rechazar(id){
    usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/rechazarPropuesta.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data.trim() == "correcto"){
        $("#"+id).remove();
        if($("#contenedorSugerencias").is(':empty')){
          window.location = "aceptarSugerencia.html";
        }
      }
    }
  });
}
/**
* Function retrocederEsp
* Si se encuentra en la pagina de especificSugerencia retrocede
* a la pagina donde se encuentran las demas sugerencias
* solamente Redirige a la ruta /sugerencias
*/
function retrocederEspS(){
  window.location = "aceptarSugerencia.html";
}
//______________________________Seccion Reevaluacion

/**
* Function reevaluacion
* Obtiene las relaciones marcadas como rechazadas
* Las muestra en la pantalla
* Si no existen rechazadas muestra una imagen
*/
$(document).ready(function(){
    if($("#reevaluacion").length){
        usuario = localStorage.getItem('id_usuario');
      $.ajax({
        url: 'Backend/reevaluacionPropuestas.php',
        type:'GET',
          data:{usuario:usuario},
        success: function(data){

            data = JSON.parse(data);
          var con = "";
          if(data.length > 0){
            for(palabras in data){
              con += "<div  onclick=\"cambiarRee("+data[palabras][0]+", \'"+data[palabras][5]+"\')\" class=' col-sm-5 col-md-5 col-lg-4 respuesta sugeridas'>"+
              "<div class='banderaP'><img src='img/banderas/"+data[palabras][2]+"' alt='bandera' class='banderaEspe'/></div>"+
                "<div class='text-left pais2'><h1>"+data[palabras][1]+"</h1></div>"+
                "<div class='text-center aceptar'></div></div>";

            }
          }else{
            con = "<div class='text-center empty'>"+
              "<img src='img/empty.png' alt='empty' />"+
              "<h3>Nada por Reevaluar</h3></div>";
          }
          $("#respuestas").html(con);
        }
      });
    }
});
/**
* Function cambiarRee
* @param id
* Redirige a la ruta reevaluacion/especifica
* con el id de la palabra seleccionada
*/
function cambiarRee(id, tipo){
    sessionStorage.setItem("idPalabra",id);
    sessionStorage.setItem("tipo_re",tipo);
    window.location = "especificReevaluacion.html";
}


function obtener_datos_relacion(relacion_id, palabra){
  contenido="";
  $.ajax({
    url:'Backend/obtener_datos_relacion.php',
    type:'GET',
    data:{relacion_id:relacion_id},
    success:function(data){
      console.log(data);
      if (data.trim() != "[]") {
        data = JSON.parse(data.trim());
        contenido += "<h3 class='text-center reportar_relacion'>Razones por las que se reporto "+palabra+"</h3><p>"+data[0][0]+"</p><h6>Reporte realizado por <b>"+data[0][1]+"</b></h6>"+
        "<p>"+data[1][0]+"</p><h6>Reporte realizado por <b>"+data[1][1]+"</b></h6>"+
        "<p>"+data[2][0]+"</p><h6>Reporte realizado por <b>"+data[2][1]+"</b></h6>";
        $("#reportes_relacion_datos").html(contenido);
      } else {
        $("#reportes_relacion_datos").html("<h3 class='text-center'>Esta relación no tiene reportes</h3>");
      }

    }
  });
}


function inicializar_contenedor_reportes(){
  var swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      spaceBetween: 30,
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
}

/**
* Function palabras
* Obtiene el detalle de la relación que sea rechazada
*/
$(document).ready(function(){
  if($("#palabraPadre2").length){
    ancho = Math.max(document.documentElement.clientWidth, window.innerWidth);
    if (ancho > 1095){
      sin_swiper();
    }
    else {
      con_swiper();
    }
  }
});
function con_swiper(){
    // self.palabra("Cargando...");
    usuario = localStorage.getItem('id_usuario');
    var id = sessionStorage.getItem("idPalabra");
    var tipo = sessionStorage.getItem("tipo_re");
  if(id != ""){
    $.ajax({
      url: 'Backend/obtenerPropuestasEspecificoRe.php',
      type:'GET',
      data: {id:id, usuario:usuario, tipo_re:tipo},
      success: function(data){

        data = JSON.parse(data);
        var def1 = "";
        var def2 = "";
        var def3 = "";
        var con = "";
        var rep1 = "";
        var rep2 = "";
        var rep3 = "";
        var us1 = "";
        var us2 = "";
        var us3 = "";

        def1 += data[0][0][5];
        def2 += data[0][0][6];
        def3 += data[0][0][7];
        rep1 += data[1][0];
        rep2 += data[1][2];
        rep3 += data[1][4];
        us1 += data[1][1];
        us2 += data[1][3];
        us3 += data[1][5];
        if (data[0][1] != undefined) {
          con = "<div class='contenedor_reportes_relaciones col-lg-12 '>"+
            "<div id='division_relacion' class='division_relacion col-md-6 col-sm-6  col-xs-12 col-lg-6'>"+
            "<div class='cont_grand swiper-container'><div class='swiper-wrapper'>";

          for(datos in data[0]){
              if(datos == 0){

                 if (def1 == "null") {def1 = "No hay Definición";}
                 if (def2 == "null") {def2 = "No hay Definición";}
                 if (def3 == "null") {def3 = "No hay Definición";}
                 $("#definicion_ifilac2").html(def2);
                 $("#definicion_unesco2").html(def3);
                 $("#definicion_general2").html(def1);
                 $("#banderaPrincipal2").attr("src", "img/banderas/"+data[0][datos][2]);
                  $("#palabraPadre2").html(data[0][datos][1]);
                  $("#palabraPadreId").val(data[0][datos][0]);
                  $("#sTitulo").attr("onclick", "hablar('"+data[0][datos][1]+"',this,1)");

                }else{
                    // \"cambiarRee("+data[palabras][0]+", \'"+data[palabras][5]+"\')\"
                    con = con + "<div class='swiper-slide'><div onclick=\"obtener_datos_relacion("+data[0][datos][5]+",\'"+data[0][datos][1]+"\')\" class='tarjeta-reporte col-xs-12 col-sm-12 col-md-12 col-lg-5 acomodarR' id='"+data[0][datos][5]+"'>"+
                    "<div class='banderas'><img src='img/banderas/"+data[0][datos][2]+"' alt='bandera' class='banderaEspe'/></div>"+
                     "<div class='text-left pais2'><h2  class='text-left'>"+data[0][datos][1]+"</h2></div>"+
                     "<i onclick='aceptarRe("+data[0][datos][5]+")' style='color:#56B892' class='fa fa-check-circle-o' aria-hidden='true'></i>"+
                     "<i onclick='rechazarRe("+data[0][datos][5]+")' style='color:#EC575D' class='fa fa-times-circle-o' aria-hidden='true'></i></div></div>";
                    // "<img src='img/aceptar.png' alt='aceptar' onclick='aceptarRe("+data[datos][5]+")' class='opcionesSug aceptar'/>"+
                    // "<img src='img/cancel.png' alt='rechazar' onclick='rechazarRe("+data[datos][5]+")' class='opcionesSug rechazar'/></div>";
                }
          }
          con += "</div>"+
  "<div class='pag-report swiper-pagination'></div>"+
  "</div></div>"+
            "<div id='reportes_relacion_datos' class='datos-reporte_relacion col-md-6 col-sm-6 col-xs-12 col-lg-6'>"+
            "<h3 class='reportar_relacion' >Selecciona una relacion para ver sus reportes</h3>"+
            "</div>"+
          "</div>";
        } else {
          for(datos in data[0][0]){
              if(datos == 0){
                if (def1 == "null") {def1 = "No hay Definición";}
                if (def2 == "null") {def2 = "No hay Definición";}
                if (def3 == "null") {def3 = "No hay Definición";}
                $("#definicion_ifilac2").html(def2);
                $("#definicion_unesco2").html(def3);
                $("#definicion_general2").html(def1);
                $("#banderaPrincipal2").attr("src", "img/banderas/"+data[0][datos][2]);
                $("#palabraPadre2").html(data[0][datos][1]);
                $("#palabraPadreId").val(data[0][datos][0]);
                $("#sTitulo").attr("onclick", "hablar('"+data[datos][1]+"',this,1)");
                con = "<div class='reportar col-lg-6 ' id='reportein'>"+
                  "<div class='division top col-xs-12 col-lg-10'><div class=' acep_report text-center col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8' onclick='aceptarPalabra("+id+")'>"+
                  "<i style='color:#56B892' class='fa fa-check-circle-o' aria-hidden='true'></i>"+
                    "<h3>Aceptar Palabra</h3>"+
                 "</div></div>"+
                  "<div class='division col-xs-12 col-lg-10'><div class=' acep_report text-center col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8  ' onclick='rechazarPalabra("+id+")'>"+
                    "<i  style='color:#EC575D' class='fa fa-times-circle-o' aria-hidden='true'></i>"+
                    "<h3>Rechazar Palabra</h3></div>"+
                  "</div>"+
                  "</div>"+
                  "<div class='datos-reporte text-center col-lg-6'>"+
                  "<h3>Razones por las que se reporto esta palabra</h3>"+
                  "<p><h5>Reporte realizado por <b>"+us1+"</b></h5>"+rep1+"<h5>Reporte realizado por <b>"+us2+"</b></h5>"+rep2+"<h5>Reporte realizado por <b>"+us3+"</b></h5>"+rep3+"</p>"+

                  "</div>"
                "</div>";
                }
          }
        }
        $("#contenedorSugerencias").html(con);
        inicializar_contenedor_reportes();
      }
    });
    }
}
function sin_swiper(){
  $(document).ready(function(){
  if($("#palabraPadre2").length){
      // self.palabra("Cargando...");
      usuario = localStorage.getItem('id_usuario');
     var id = sessionStorage.getItem("idPalabra");
      var tipo = sessionStorage.getItem("tipo_re");
    if(id != ""){
      $.ajax({
        url: 'Backend/obtenerPropuestasEspecificoRe.php',
        type:'GET',
        data: {id:id, usuario:usuario, tipo_re:tipo},
        success: function(data){
          data = JSON.parse(data);
          var def1 = "";
          var def2 = "";
          var def3 = "";
          var con = "";
          var rep1 = "";
          var rep2 = "";
          var rep3 = "";
          var us1 = "";
          var us2 = "";
          var us3 = "";

          def1 += data[0][0][5];
          def2 += data[0][0][6];
          def3 += data[0][0][7];
          rep1 += data[1][0];
          rep2 += data[1][2];
          rep3 += data[1][4];
          us1 += data[1][1];
          us2 += data[1][3];
          us3 += data[1][5];
          if (data[0][1] != undefined) {
            con = "<div class='contenedor_reportes_relaciones col-lg-12 '>"+
              "<div id='division_relacion' class='division_relacion col-md-6 col-sm-6  col-xs-12 col-lg-6'>";

            for(datos in data[0]){
                if(datos == 0){

                   if (def1 == "null") {def1 = "No hay Definición";}
                   if (def2 == "null") {def2 = "No hay Definición";}
                   if (def3 == "null") {def3 = "No hay Definición";}
                   $("#definicion_ifilac2").html(def2);
                   $("#definicion_unesco2").html(def3);
                   $("#definicion_general2").html(def1);
                   $("#banderaPrincipal2").attr("src", "img/banderas/"+data[0][datos][2]);
                    $("#palabraPadre2").html(data[0][datos][1]);
                    $("#palabraPadreId").val(data[0][datos][0]);
                    $("#sTitulo").attr("onclick", "hablar('"+data[0][datos][1]+"',this,1)");

                  }else{
                      // \"cambiarRee("+data[palabras][0]+", \'"+data[palabras][5]+"\')\"
                      con = con + "<div onclick=\"obtener_datos_relacion("+data[0][datos][5]+",\'"+data[0][datos][1]+"\')\" class='tarjeta-reporte col-xs-12 col-sm-6 col-md-6 col-lg-5 acomodarR ' id='"+data[0][datos][5]+"'>"+
                      "<div class='banderas'><img src='img/banderas/"+data[0][datos][2]+"' alt='bandera' class='banderaEspe'/></div>"+
                       "<div class='text-left pais2'><h2  class='text-left'>"+data[0][datos][1]+"</h2></div>"+
                       "<i onclick='aceptarRe("+data[0][datos][5]+")' style='color:#56B892' class='fa fa-check-circle-o' aria-hidden='true'></i>"+
                       "<i onclick='rechazarRe("+data[0][datos][5]+")' style='color:#EC575D' class='fa fa-times-circle-o' aria-hidden='true'></i></div>";
                      // "<img src='img/aceptar.png' alt='aceptar' onclick='aceptarRe("+data[datos][5]+")' class='opcionesSug aceptar'/>"+
                      // "<img src='img/cancel.png' alt='rechazar' onclick='rechazarRe("+data[datos][5]+")' class='opcionesSug rechazar'/></div>";
                  }
            }
            con += "</div>"+
              "<div id='reportes_relacion_datos' class='datos-reporte_relacion col-md-6 col-sm-6 col-xs-12 col-lg-6'>"+
              "<h3 class='reportar_relacion' >Selecciona una relacion para ver sus reportes</h3>"+
              "</div>"+
            "</div>";
          } else {
            for(datos in data[0][0]){
                if(datos == 0){
                  if (def1 == "null") {def1 = "No hay Definición";}
                  if (def2 == "null") {def2 = "No hay Definición";}
                  if (def3 == "null") {def3 = "No hay Definición";}
                  $("#definicion_ifilac2").html(def2);
                  $("#definicion_unesco2").html(def3);
                  $("#definicion_general2").html(def1);
                  $("#banderaPrincipal2").attr("src", "img/banderas/"+data[0][datos][2]);
                  $("#palabraPadre2").html(data[0][datos][1]);
                  $("#palabraPadreId").val(data[0][datos][0]);
                  $("#sTitulo").attr("onclick", "hablar('"+data[datos][1]+"',this,1)");
                  con = "<div class='reportar col-lg-6 ' id='reportein'>"+
                    "<div class='division top col-xs-12 col-lg-10'><div class=' acep_report text-center col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8' onclick='aceptarPalabra("+id+")'>"+
                    "<i style='color:#56B892' class='fa fa-check-circle-o' aria-hidden='true'></i>"+
                      "<h3>Aceptar Palabra</h3>"+
                   "</div></div>"+
                    "<div class='division col-xs-12 col-lg-10'><div class=' acep_report text-center col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8  ' onclick='rechazarPalabra("+id+")'>"+
                      "<i  style='color:#EC575D' class='fa fa-times-circle-o' aria-hidden='true'></i>"+
                      "<h3>Rechazar Palabra</h3></div>"+
                    "</div>"+
                    "</div>"+
                    "<div id='datos-reporte_id' class='datos-reporte text-center col-lg-6'>"+
                    "<h3>Razones por las que se reporto esta palabra</h3>"+
                    "<p><h5>Reporte realizado por <b>"+us1+"</b></h5>"+rep1+"<h5>Reporte realizado por <b>"+us2+"</b></h5>"+rep2+"<h5>Reporte realizado por <b>"+us3+"</b></h5>"+rep3+"</p>"+

                    "</div>"
                  "</div>";
                  }
            }
          }


          $("#contenedorSugerencias").html(con);
          if (data[1][0] == undefined) {
            $("#datos-reporte_id").html("<h3>Esta palabra no tiene reportes </h3>");
          }

        }
      });
    }
  }
  });
}


function aceptarPalabra(id){
    usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/aceptarPalabra.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data.trim() == "correcto"){
        window.location = "aceptarReevaluacion.html";
      }
    }
  });
}

/**
* Function rechazar
* Si se rechaza la relación se manda un ajax a la ruta /rechazarPropuestas
* Si retorna un correcto se elimina el div del contenedorSugerencias
* Si el div queda vacio se retorna a /sugerencias
* @param id
*/
function rechazarPalabra(id){
    usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/rechazarPalabra.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data.trim() == "correcto"){
          window.location = "aceptarReevaluacion.html";
      }
    }
  });
}


/**
* Function aceptar
* Si se acepta la relación se manda un ajax a la ruta /aceptarPropuestas
* Si retorna un correcto se elimina el div del contenedorSugerencias
* Si el div queda vacio se retorna a /reevaluacion
* @param id
*/
function aceptarRe(id){
  var idE = "#"+id;
  usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/aceptarPropuestaRe.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){

      if(data.trim() == "correcto"){
        $(idE).remove();
        if($("#division_relacion").is(':empty')){
          window.location = "aceptarReevaluacion.html";
        }
      }
    }
  });
}

/**
* Function rechazar
* Si se rechaza la relación se manda un ajax a la ruta /rechazarPropuestas
* Si retorna un correcto se elimina el div del contenedorSugerencias
* Si el div queda vacio se retorna a /reevaluacion
* @param id
*/
function rechazarRe(id){
  var idE = "#"+id;
    usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/rechazarPropuestaRe.php',
    type:'GET',
    data: {id:id, usuario:usuario},
    success: function(data){
      if(data.trim() == "correcto"){
        $(idE).remove();
        if($("#division_relacion").is(':empty')){
          window.location = "aceptarReevaluacion.html";
        }
      }
    }
  });
}
/**
* Function retrocederEsp
* Redirige a la ruta /reevaluacion
* Para mostrar las demas rechazadas
*/
function retrocederEsp(){
  window.location = "aceptarReevaluacion.html";
}
/********Reportar Palabras***/
/**
* Function ocultarReporte
* Oculta el div del reporte
*/
function ocultarReporte(){
  $("#reporte").css('visibility','hidden');
  $("#reporte").css('opacity','0');
  if($("#definicion").css('visibility')=='visible'){
    $("#definicion").css('visibility','hidden');
  }
  $("#reportein").css('visibility','hidden');
  $("#reportes").css('visibility','hidden');
}

/**
* Mostrar reportar
* Muestra el div para reportar relacion
* cuando se presiona el div sobre 1 seg
*/
$(document).ready(function(){
  $("#resultadosBusqueda").hammer({domEvents:true}).on("press", "div", function() {
    $("#reporte").css('visibility','visible');
    $("#reporte").css('opacity','1');
    $("#reportein").css('visibility','visible');
    $("#idDin").val(this.id);
    $("#idreP").val(this.id);
  });
});
function mostrarReportar(){
  contenido = "<div class='report-un col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 ' id='palabra_report2'>"+
    "<i class='fa fa-times-circle report-icon'></i>"+
    "<h3>¿Por qué quieres reportar esta palabra?</h3>"+
    "<input type='hidden' name='idDin' id='idreP'>"+
    "<textarea id='reporte-texto' class='reporte' rows='4' cols='50' maxlength='150'></textarea><br>"+
    "<button class='btn btn-primary boton' onclick='reportarUnaPalabra();'>Reportar</button>"+
  "</div>";
  $("#reportes").html(contenido);
  $("#reporte").css('visibility','visible');
    $("#reporte").css('opacity','1');
    $("#reportes").css('visibility','visible');
    $("#idDin").val(this.id);
    $("#idreP").val(this.id);

}

/**
* Function reportar
* Envia el id de la palabra a reportar a la ruta index/reportarPalabra
* Si reotrna un correcto oculta las opciones de reportar y muestra un mensaje
* de palabra reportada
*/
function reportarRelacion() {
  idPalabra = sessionStorage.getItem("idPalabra");
  usuario = localStorage.getItem('id_usuario');
  if (usuario == null) {
    self.palabra("Necesitas iniciar sesión para reportar una relación");
  } else {

    descripcion = $("#reporte-relacion").val();
    if (descripcion == ""){
      self.palabra("Completar campos");
    }else{
  var id = $("#idDin").val();
  $.ajax({
    url: 'Backend/reportarPalabra.php',
    type:'GET',
    data: {id:id, idPalabra:idPalabra, usuario:usuario, descripcion:descripcion},
    success: function(data){
      if(data.trim() == "correcto"){
        $("#reporte-relacion").val("");
        self.ocultarReporte();
        self.palabra("Relación reportada");
      } else if (data.trim() == "correcto+3") {
        $("#reporte-relacion").val("");
        self.ocultarReporte();
        self.palabra("Relación reportada");
          setTimeout(function(){  window.location = "resultados.html";},2000);

      }
    }
  });
  }
}
}

// function reportarUnaPalabraRelacionada(){
//   idPalabra = $("#idDin").val();
//   usuario = localStorage.getItem('id_usuario');
//   $.ajax({
//     url: 'Backend/reportarUnaPalabra.php',
//     type:'GET',
//     data: {id:idPalabra,usuario:usuario},
//     success: function(data){
//       if(data == "correcto"){
//         self.ocultarReporte();
//         self.palabra("Palabra reportada");
//       } else if (data == "correcto+3") {
//         self.ocultarReporte();
//         self.palabra("Palabra reportada");
//           setTimeout(function(){  window.location = "resultados.html";},2000);
//
//       }
//     }
//   });
// }

function reportarUnaPalabra(){
  idPalabra = sessionStorage.getItem("idPalabra");
  usuario = localStorage.getItem('id_usuario');

  if (usuario == null) {
    self.palabra("Necesitas iniciar sesión para reportar una palabra");
  } else {
    descripcion = $("#reporte-texto").val();
    if (descripcion == ""){
      self.palabra("Completar campos");
    }else{
    $.ajax({
      url: 'Backend/reportarUnaPalabra.php',
      type:'GET',
      data: {id:idPalabra,usuario:usuario,descripcion:descripcion},
      success: function(data){
        if(data.trim() == "correcto"){
          self.ocultarReporte();
          self.palabra("Palabra reportada");
        } else if (data.trim() == "correcto+3") {
          self.ocultarReporte();
          self.palabra("Palabra reportada");
            setTimeout(function(){  window.location = "palabras.html";},2000);

        }
      }
    });
  }
  }

}

function agregar_imagenes(){
  usuario = localStorage.getItem('id_usuario');
  if (usuario == null) {
    self.palabra("Necesitas iniciar sesión para subir imagenes");
  } else {
    contenido = "<form enctype='multipart/form-data' id='img_palabra' onsubmit='return false' method='POST'>"+
      "<input onchange='cargar_foto()' style='display:none' type='file' id='images_palabras' name='images_palabras' multiple></form>";
    $("#reportes").html(contenido);
    $("#images_palabras").click();
  }

}

function cargar_subir_img(){
  idPalabra = sessionStorage.getItem("idPalabra");
  usuario = localStorage.getItem('id_usuario');
  $.ajax({
    url: 'Backend/comprobar_imagen_palabra.php',
    type:'GET',
    data: {palabra_id:idPalabra,usuario:usuario},
    success: function(data){
      console.log(data);
      if (data.trim() != "subir_imagen") {
        $("#reporte").css('visibility','visible');
        $("#reporte").css('opacity','1');
        $("#reportes").css('visibility','visible');
        data = JSON.parse(data);
        cont = "<div class='swiper-container'>"+
        "<div class='swiper-wrapper'>";
          for (var i = 0; i < data[0].length; i++) {
            if  (data[1][i][0] == "" || data[1][i][1] == ""){
               data[1][i][0] = "No hay registro";
               data[1][i][1] = "No hay registro";
              cont += "<div class='swiper-slide'><div class='grande col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 ' id='palabra_report2'>"+
                 "<div class='modal_der col-xs-12 col-md-6 col-sm-6 col-lg-8'>"+
                 "<img id='' class='img_vista ' src='img/imgPalabras/"+idPalabra+"/"+data[0][i]+"'>"+
                 "</div>"+
                 "<div class='modal_izq  visor-img  col-xs-12 col-md-8  col-sm-8 col-lg-6'>"+
                 "<div class='texto-centrado'>"+
                 "<h5><b>Autor Foto</b>: "+data[1][i][0]+"</h5>"+
                 "<h5><b> Subida Por</b>: "+data[1][i][2]+"</h5>"+
                 "<h5><b>Descripción</b>: "+data[1][i][1]+"</h5>"+
                 "</div>"+
                 "<button class='btn btn-primary' style='top:0 !important' onclick=\"reportarImagen(\'"+data[1][i][3]+"\');\">Reportar</button>"+
                 "</div>"+
                 "</div></div>";
              // cont += "<div class='swiper-slide'><img class='col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4' src='img/imgPalabras/"+idPalabra+"/"+data[i]+"'/></div>";
            }else{
              cont += "<div id='contendor_img"+data[1][i][3]+"' class='swiper-slide'><div class='grande col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 ' id='palabra_report2'>"+
                 "<div class='modal_der col-xs-12 col-md-6 col-sm-6 col-lg-8'>"+
                 "<img id='' class='img_vista ' src='img/imgPalabras/"+idPalabra+"/"+data[0][i]+"'>"+
                 "</div>"+
                 "<div class='modal_izq  visor-img  col-xs-12 col-md-8  col-sm-8 col-lg-6'>"+
                 "<div class='texto-centrado'>"+
                 "<h5><b>Autor Foto</b>: "+data[1][i][0]+"</h5>"+
                 "<h5><b> Subida Por</b>: "+data[1][i][2]+"</h5>"+
                 "<h5><b>Descripción</b>: "+data[1][i][1]+"</h5>"+
                 "</div>"+
                 "<button class='btn btn-primary' style='top:0 !important' onclick=\"reportarImagen(\'"+data[1][i][3]+"\');\">Reportar</button>"+
                 "</div>"+
                 "</div></div>";
              // cont += "<div class='swiper-slide'><img class='col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4' src='img/imgPalabras/"+idPalabra+"/"+data[i]+"'/></div>";

            }

          }
        cont += "<div class='swiper-slide'><div  onclick='agregar_imagenes()' class='add_foto'><i style='color: rgba(255,255,255,.9);  font-size: 31px; margin-top: 9px;' class='fa fa-camera' aria-hidden='true'></i><h4>Agregar una foto</h4></div></div>"+
        "</div><div class='swiper-pagination'></div>"+
        "<div class='swiper-button-prev'></div>"+
        "<div class='swiper-button-next'></div></div>";
        $("#reportes").html(cont);
        inicializar_contenedor();
      } else {
        $("#reporte").css('visibility','visible');
        $("#reporte").css('opacity','1');
        $("#reportes").css('visibility','visible');

        contenido = "<div  onclick='agregar_imagenes()' class='add_foto'>"+
        "<i style='color: rgba(255,255,255,.9);  font-size: 31px; margin-top: 9px;' class='fa fa-camera' aria-hidden='true'></i><h4>Agregar una foto</h4>"+
        "</div>";
        $("#reportes").html(contenido);
      }
    }
  });
}


function reportarImagen(imagen_id){
  $.ajax({
    url:'Backend/reportarImagen.php',
    type:'GET',
    data:{imagen_id:imagen_id},
    success:function(data){

      if (data.trim() == "imagen reportada") {
        self.palabra("Imagen reportada");
      } else if (data.trim() == "imagen eliminada") {
        $("#contendor_img"+imagen_id).remove();
        inicializar_contenedor();
        // setTimeout(function(){
        //
        // }, 2000);
      }

    }
  });
}


function inicializar_contenedor(){
  var swiper = new Swiper('.swiper-container', {
    effect: 'flip',
    grabCursor: true,

    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  });
}


function abrir_modal_imgen(imagen){
  conte ="";
  conte += "<div class='grande col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 ' id='palabra_report2'>"+
    "<div class='modal_der col-xs-12 col-md-6 col-sm-6 col-lg-6'>"+
    "<img id='ver_imagen' class='' src=''>"+
    "</div>"+
    "<div class='modal_izq col-xs-12 col-md-5 col-sm-8 col-lg-6'>"+
    "<h5>Agregar autor de la imagen</h5>"+
    "<input class='autor' id='autor'>"+
    "<h5>Agregar descripción Imagen</h5>"+
    "<textarea id='descripcion_imagen' class='img-info' rows='4' cols='50' maxlength='150'></textarea>"+
    "<br><button onclick='guardar_img_palabras()' class='btn btn-primary'>Agregar Imagen</button>"+
    "</div>"+
    "</div>";
  $("#reportes").append(conte);
  cambiar_imagen(imagen);
}

function cargar_foto() {
  var val = $("#images_palabras").val();
  var file_type = val.substr(val.lastIndexOf('.')).toLowerCase();
  if (file_type  === '.jpg') {
      self.abrir_modal_imgen($("#images_palabras").get(0));
      // guardar_img_palabras();
  }else{
      $("#images_palabras").val("");
  self.palabra("Subir archivo .jpg");
  }
}

function cambiar_imagen(input){
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#ver_imagen').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
  }
}

function guardar_img_palabras() {
  var file = $("#images_palabras").val();
  var formData = new FormData($('#img_palabra')[0]);
  var idPalabra = sessionStorage.getItem("idPalabra");
  var idUser = localStorage.getItem("id_usuario");
  var autor = $("#autor").val();
  var descripcion = $("#descripcion_imagen").val();
  formData.append("palabra_id", idPalabra);
  formData.append("idUser", idUser);
  formData.append("autor", autor);
  formData.append("descripcion", descripcion);
  if(file != ""){
    self.palabra("Guardando cambios");
    $.ajax({
      url: 'Backend/guardar_imagenes_palabras.php',
      type: 'POST',
      processData : false,
      contentType: false,
      data: formData,
      success: function(data){
        ocultarReporte();
      }
    });
  } else{
      self.palabra("Ingresar una imagen");
  }
}
